module Main where

import NixFromCabal.Types
import NixFromCabal.Description (description)
import NixFromCabal
import qualified Distribution.Version as HV
import qualified Distribution.Package as HP
import qualified Distribution.Text as HT

import Control.Monad
import Control.Monad.Extra
import Control.Applicative

import System.Process
import System.Environment (getArgs)

import Data.Monoid
import qualified Data.Text as T

import Nix.Expr
import Nix.Pretty

import Options.Applicative
import Options.Applicative.Builder

main :: IO ()
main = execParser parser >>= id
  where
    parser = info (helper <*> opts)
      (fullDesc
        <> header hdr
        <> progDesc description)
   
    opts = pure nixFromCabal
      <*> ( (,) <$> packageOpt <*> optional versionOpt )
      <*> optional outOpt

    hdr = "nix-to-cabal: compute a precise\
          \ nix expression from a given cabal file"


packageOpt :: Parser HP.PackageName
packageOpt = option (HP.mkPackageName <$> str)
  ( short 'p' <> long "package" <> metavar "PACKAGE_NAME" 
  <> help "The (hackage) name of a package for which\
          \ you want a nix expression." )

versionOpt :: Parser HV.Version
versionOpt = option versionP
  ( short 'v' <> long "version" <> metavar "PACKAGE_VERSION"
  <> help "Optionally the version of a package, for which\
          \ you want a nix expression." )
  where
    versionP = do
      s <- str
      case HT.simpleParse s of
        Nothing -> fail "Could not parse version."
        Just ver -> pure ver

outOpt :: Parser FilePath
outOpt = option str
  ( short 'o' <> long "out" <> metavar "OUTFILE"
  <> help "Specify an output file instead\
          \ of writing to stdout." )

nixFromCabal :: (HP.PackageName,Maybe HV.Version) -> Maybe FilePath -> IO ()
nixFromCabal request mbOutfile = do
  readProcess "cabal" ["update"] ""
  expr <- uncurry createPackageNExpr request
  -- putStrLn "with (import <nixpkgs> {}).haskellPackages;"
  output . show $ prettyNix expr
  where
    output
      | Just outfile <- mbOutfile = writeFile outfile
      | otherwise = putStr


