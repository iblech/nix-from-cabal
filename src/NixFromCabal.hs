{-# language OverloadedStrings #-}
module NixFromCabal
  ( createPackageNExpr )
where

import NixFromCabal.Types
import NixFromCabal.Cabal
import NixFromCabal.Hackage
import qualified Distribution.Package as HP
import qualified Distribution.Version as HV
import qualified Distribution.Text as HT

import Control.Monad
import Control.Monad.Extra
import Control.Applicative

import Data.Semigroup
import Data.Maybe
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T

import Nix.Expr
import Nix.Pretty


toText :: HT.Text a => a -> T.Text
toText = T.pack . HT.display



callHackage :: Package -> [Binding NExpr] -> NExpr
callHackage (name,version) bindings =
  mkSym "callHackage"
    @@ mkStr (toText name)
    @@ mkStr (toText version)
    @@ mkNonRecSet bindings


createPackageNExpr :: HP.PackageName
  -> Maybe HV.Version -> IO NExpr
createPackageNExpr name mbVersion = do
  deps <- getRealTransitiveDependencies name mbVersion
  depTable <- getDepTableFromHackage name mbVersion
  let bindings = depTableToNBindings deps depTable
  pure $ mkLets bindings (mkSym $ toText name)


depTableToNBindings :: M.Map HP.PackageName HV.Version
  -> DepTable -> [Binding NExpr]
depTableToNBindings packages depTable = 
  mkBinding <$> M.toList packages
  where
    mkBinding :: Package -> Binding NExpr
    mkBinding pkg@(name,_) =
      let mkInherit bs = if null bs then [] else [inherit bs]
       in bindTo (toText name)
        . callHackage pkg 
        . mkInherit
        $ inherited name

    inherited :: HP.PackageName -> [NKeyName NExpr]
    inherited name = 
       map (StaticKey . toText)
       . filter (`M.member` packages)
       . S.toList
       . fromMaybe (noPackageErr name)
       $ M.lookup name depTable 

    noPackageErr name = error
      $ "package " <> HT.display name
      <> " is missing from hackage-db\
         \ dependencies (dumping table):\n\n"
      <> show depTable


