module NixFromCabal.Types where

import qualified Distribution.Version as HV
import qualified Distribution.Package as HP

type Package = (HP.PackageName,HV.Version)

data Tree a = Tree a [Tree a]
  deriving (Eq,Ord,Show)



