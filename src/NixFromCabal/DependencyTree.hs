module NixFromCabal.DependencyTree
 ( getDependencyTree )
where 

import NixFromCabal.Types
import NixFromCabal.Hackage
import NixFromCabal.Cabal

import qualified Distribution.Package as HP
import qualified Distribution.Version as HV
import qualified Distribution.Text as HT

import qualified Data.Map as M
import qualified Data.Set as S

import Data.Semigroup
import Data.Maybe



getDependencyTree :: HP.PackageName
  -> Maybe HV.Version -> IO (Tree Package)
getDependencyTree name mbVersion = do
  deps <- getRealTransitiveDependencies name mbVersion
  depTable <- getDepTableFromHackage name mbVersion
  let Just tree = computeDepTree name deps depTable
  pure tree


computeDepTree :: HP.PackageName
  -> M.Map HP.PackageName HV.Version
  -> DepTable -> Maybe (Tree Package)
computeDepTree rootName realdeps depTable =
  build rootName
  where
    build name =
      let withVer ver = Tree (name,ver)
            . catMaybes . map build . S.toList $
            ( fromMaybe (notFoundErr name depTable)
              $ M.lookup name depTable )
       in withVer <$> M.lookup name realdeps 

    notFoundErr :: HP.PackageName -> a
    notFoundErr name = error
      $ "package " <> HT.display name
      <> " is missing from hackage-db\
         \ dependencies (dumping table):\n\n"
      <> show depTable

    

