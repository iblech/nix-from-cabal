module NixFromCabal.Hackage
  ( DepTable(..)
  , getDepTableFromHackage
  , getDepsFromHackageDB )
where

import Distribution.Types.CondTree
import Distribution.Package
import Distribution.PackageDescription
import Distribution.Version
import Distribution.Hackage.DB
import qualified Distribution.Version as HV
import qualified Distribution.Text as T

import qualified Data.Map as M
import qualified Data.Set as S
import Data.Maybe
import Data.Semigroup

import Control.Monad
import Control.Monad.State
import Control.Monad.Extra

type DepTable = M.Map PackageName (S.Set PackageName)

data LookupResult =
  HavePackage GenericPackageDescription
  | NoSuchPackage PackageName
  | NoSuchVersionOf PackageName Version
  deriving (Eq,Show)

lookupPackage :: PackageName
  -> Maybe Version -> HackageDB
  -> LookupResult
lookupPackage name mbVersion db =
  case M.lookup name db of
    Nothing -> NoSuchPackage name
    Just pkg -> case mbVersion of
      Nothing -> finalize . snd $ M.findMax pkg
      Just ver -> case M.lookup ver pkg of
        Nothing -> NoSuchVersionOf name ver
        Just pdesc -> finalize pdesc
  where finalize = HavePackage . cabalFile 


getHackageDB :: IO HackageDB
getHackageDB =  readTarball Nothing =<< hackageTarball


computeDependencies :: GenericPackageDescription -> [Dependency]
computeDependencies pdesc = 
  depsSetup ++ depsLib
  ++ compDeps condExecutables
  ++ compDeps condTestSuites 
  ++ compDeps condBenchmarks
  where
    compDeps :: ( GenericPackageDescription
                  -> [(a, CondTree v [Dependency] b)] )
             -> [Dependency]
    compDeps = (>>= condTreeToDependencies . snd) . ($pdesc)
    
    depsLib :: [Dependency]
    depsLib = (>>= condTreeToDependencies)
                . maybeToList . condLibrary $ pdesc
    
    depsSetup :: [Dependency]
    depsSetup = (>>= setupDepends)
                  . maybeToList . setupBuildInfo
                  . packageDescription $ pdesc


condTreeToDependencies :: CondTree v [Dependency] a -> [Dependency]
condTreeToDependencies tree = 
  (condTreeConstraints tree ++) $ do
    branch <- condTreeComponents tree
    condTreeToDependencies (condBranchIfTrue branch) ++
      maybe [] condTreeToDependencies (condBranchIfFalse branch)

 

getDepsFromHackageDB :: HackageDB
  -> PackageName -> Maybe HV.Version -> [PackageName]
getDepsFromHackageDB db name mbVersion = do
  pdesc <- lookupResToList $ lookupPackage name mbVersion db
  extractName <$> computeDependencies pdesc
  where
    lookupResToList res = case res of
      HavePackage pdesc -> [pdesc]
      NoSuchPackage name -> []
        -- ^^ Ignore deps which are not on hackage
        -- ^^ TODO: Should we log these ?
      NoSuchVersionOf name ver -> verErr name ver
 
    extractName (Dependency name _) = name
    
    verErr name ver = error $ "version " <> pretty ver
           <> " of package " <> pretty name <> " not found"

    pretty :: T.Text a => a -> String
    pretty = show . T.disp



getDepTableFromHackage :: PackageName -> Maybe HV.Version
  -> IO DepTable
getDepTableFromHackage name mbVersion = do
  db <- getHackageDB
  execStateT (build db name mbVersion) M.empty
  where
    build :: HackageDB -> PackageName
          -> Maybe HV.Version -> StateT DepTable IO ()
    build db name mbVersion = do
      let deps = getDepsFromHackageDB db name mbVersion
      modify $ M.insert name (S.fromList deps)
      forM_ deps $ \dep ->
        unlessM (M.member dep <$> get)
                (build db dep Nothing)
 


