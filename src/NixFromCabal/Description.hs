{-# language TemplateHaskell #-}
module NixFromCabal.Description where

import NixFromCabal.TH (embedFile)

description = $(embedFile "description.txt")

