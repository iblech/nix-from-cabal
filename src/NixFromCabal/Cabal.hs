{-# language OverloadedStrings, ViewPatterns #-}
module NixFromCabal.Cabal
  ( getRealTransitiveDependencies )
where

import NixFromCabal.Types

import qualified Distribution.Version as HV
import qualified Distribution.Package as HP
import qualified Distribution.Text as HT

import Control.Monad
import Control.Monad.Extra
import Control.Applicative

import System.IO.Temp
import System.Process
import System.Directory
import System.FilePath

import Data.Semigroup
import Data.Bifunctor
import Data.Maybe
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.List as L
import qualified Data.Char as C



both :: Bifunctor p => (a -> b) -> p a a -> p b b
both f = bimap f f


getRealTransitiveDependencies :: HP.PackageName
  -> Maybe HV.Version -> IO (M.Map HP.PackageName HV.Version)
getRealTransitiveDependencies name mbVersion =
  withSystemTempDirectory "nix-from-cabal"
    $ \tmpDir -> do
      initSandbox tmpDir
      let emptyDB = tmpDir </> "empty-db"
      createDirectoryIfMissing False emptyDB
      postProcess <$> readCreateProcess
        ( proc "cabal"
          [ "--package-db=clear"
          , "install"
          , "--dry-run"
          , "--package-db=" <> emptyDB
          , packageString ] )
        { cwd = Just tmpDir } ""
  where
    packageString = HT.display name
      <> case mbVersion of
        Nothing -> ""
        Just ver -> "-" <> HT.display ver

    initSandbox tmpDir =
      readCreateProcess
        ( proc "cabal" ["sandbox", "init"] )
        { cwd = Just tmpDir } ""

    postProcess = M.fromList . parseDeps . T.pack


parseDeps :: T.Text -> [Package]
parseDeps s = case T.stripPrefix prefix s of
  Just d -> map parseLine $ T.lines d
  Nothing -> error $ "Failed to parse cabal output:\n\n" <> T.unpack s
  where
    prefix = "Resolving dependencies...\n\
             \In order, the following would be installed\
             \ (use -v for more details):\n"
    parseLine = parsePackage . T.takeWhile (not . C.isSpace)

parsePackage :: T.Text -> Package
parsePackage s = case T.span isVersion $ T.reverse s of
  (rversion, T.uncons -> Just ('-',rname)) -> 
    bimap HP.mkPackageName (fromJust . HT.simpleParse)
    $ both (T.unpack . T.reverse) (rname,rversion)
  _ -> error $ "Failed to parse package description:\n\n" <> T.unpack s
  where
    isVersion c = C.isNumber c || c == '.'









