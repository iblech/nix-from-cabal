module NixFromCabal.TH
  ( embedFile )
where

import Language.Haskell.TH
import Language.Haskell.TH.Syntax


embedFile :: FilePath -> Q Exp
embedFile path = do
  qAddDependentFile path
  ListE . map (LitE . CharL) <$> runIO (readFile path)


