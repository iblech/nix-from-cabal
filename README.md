# nix-to-cabal: compute a precise nix expression from a given cabal file

## What it is

This program is meant to produce a precise nix expression from a given cabal file.
Precise here means, that the packages will have the exact same versions, that cabal
would infer, had you tried to build the package in a fresh sandbox.
At the moment, this program only works with packages from hackage (you cannot pass
a cabal file directly, instead you specify a package name and version).


